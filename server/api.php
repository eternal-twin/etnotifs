<?php
header('content-type:application/json');

$sitesCaracs 	   = json_decode(file_get_contents('config.json'), true); 
$waitingForPlayers = json_decode(file_get_contents('data/waitingForPlayers.json'), true);

$json = [];
foreach($waitingForPlayers as $siteAlias=>$amount) {
	$json[$siteAlias] = [
		'siteName' 	=> $sitesCaracs[$siteAlias]['siteName'],
		'siteUrl' 	=> $sitesCaracs[$siteAlias]['siteUrl'],
		'nbrPlayersNeeded' => $amount
	];
}

echo json_encode(
	[
		'date'	=> date('c'),
		'waitingForPlayers' => $json
	]
);
?>